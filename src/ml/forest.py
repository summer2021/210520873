import datetime

import pandas as pd
import pydot as pydot
from sklearn import metrics
from sklearn.metrics import r2_score
from sklearn.tree import export_graphviz
import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split, GridSearchCV
starttime = datetime.datetime.now()
pd.set_option('display.max_columns', None)
features = pd.read_csv("./data/data.csv")

print(features.head(5))

print('The shap of our features is:', features.shape)



# One-hot encode categorical features
features = pd.get_dummies(features)
print(features.head(5))

print('Shape of features after one-hot encoding:', features.shape)


labels = np.array(features['ll'])

features = features.drop('ops', axis=1)
features = features.drop('opss', axis=1)
features = features.drop('throughput', axis=1)
features = features.drop('reads', axis=1)
features = features.drop('writes', axis=1)
features = features.drop('latency', axis=1)
features = features.drop('latencys', axis=1)
features = features.drop('ll', axis=1)
# features = features.drop('queue_ramp_up_period', axis=1)
# features = features.drop('read_expire', axis=1)

feature_list = list(features.columns)

features = np.array(features)

# Using Skicit-learn to split data into training and testing sets

train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size=0.25,
                                                                            random_state=42)

print('Training Features Shape:', train_features.shape)
print('Training Labels Shape:', train_labels.shape)
print('Testing Features Shape:', test_features.shape)
print('Testing Labels Shape:', test_labels.shape)



# Import the model we are using
rf = RandomForestRegressor(n_estimators=250, random_state=42,oob_score=True)

# Train the model on training data

rf.fit(train_features, train_labels)

# Use the forest's predict method on the test data
predictions = rf.predict(test_features)
print('R平方：', r2_score(test_labels , predictions))

# Calculate the absolute errors
errors = abs(predictions - test_labels)

# Print out the mean absolute error (mae)
print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')

# Calculate mean absolute percentage error (MAPE)
mape = 100 * (errors / test_labels)

# Calculate and display accuracy
accuracy = 100 - np.mean(mape)
print('Accuracy:', round(accuracy, 2), '%.')


'''

# 打印树
# Limit depth of tree to 2 levels
rf_small = RandomForestRegressor(n_estimators=10, max_depth=None, random_state=42)
rf_small.fit(train_features, train_labels)

# Extract the small tree
tree_small = rf_small.estimators_[5]

# Save the tree as a png image
export_graphviz(tree_small, out_file=
'small_tree.dot', feature_names=feature_list, rounded=True, precision=1)

(graph,) = pydot.graph_from_dot_file('small_tree.dot')

graph.write_png('small_tree.png');

'''
# 特征重要性

# Get numerical feature importances
importances = list(rf.feature_importances_)

# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]

# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)

# Print out the feature and importances
[print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];

# list of x locations for plotting
x_values = list(range(len(importances)))
# x_values = list(range(len(feature_importances)))

# Make a bar chart
plt.bar(x_values, importances, orientation='vertical')

# Tick labels for x axis
plt.xticks(x_values, feature_list, rotation='vertical')
plt.gcf().subplots_adjust(bottom=0.45)
# Axis labels and title
plt.ylabel('Importance');
# plt.xlabel('Variable');
# plt.title('Variable Importances');
plt.savefig('fdata.png')
plt.show()

